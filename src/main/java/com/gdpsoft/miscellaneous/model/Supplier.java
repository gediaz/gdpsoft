package com.gdpsoft.miscellaneous.model;

//import javax.persistence.Entity;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
@Table(schema = "miscellaneous", name = "supplier")
public class Supplier implements Serializable {

    @Id
    @SequenceGenerator(name = "supplier_sequence", sequenceName = "supplier_id_seq", schema = "miscellaneous", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "supplier_sequence")
    @Column(name = "supplier_id")
    private Long id;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "person_id")
    private Person person;

    @Column(name = "limit_credit")
    private Double limit;

    @Column(name = "active")
    private Boolean active;

    @Column(name = "created_by")
    private Integer createdBy;

    @Column(name = "register_on", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable = true, updatable = false)
    private Timestamp registerDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Double getLimit() {
        return limit;
    }

    public void setLimit(Double limit) {
        this.limit = limit;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(Timestamp registerDate) {
        this.registerDate = registerDate;
    }

    @PrePersist
    void onCreate() {
        this.setRegisterDate(new Timestamp(System.currentTimeMillis()));
    }

    @Override
    public String toString() {
        return "Supplier{" +
                "id=" + id +
                ", person=" + person +
                ", limit=" + limit +
                ", active=" + active +
                ", createdBy=" + createdBy +
                ", registerDate=" + registerDate +
                '}';
    }
}
