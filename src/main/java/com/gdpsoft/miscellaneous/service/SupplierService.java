package com.gdpsoft.miscellaneous.service;

import com.gdpsoft.miscellaneous.model.Supplier;
import com.gdpsoft.miscellaneous.repository.SupplierRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SupplierService {

    @Autowired
    private SupplierRepository supplierRepository;

    public Supplier save(Supplier client) {
        return supplierRepository.save(client);
    }

    public List<Supplier> getAll() {
        return supplierRepository.findAll();
    }

    public Optional<Supplier> get(Long id) {
        return supplierRepository.findById(id);
    }

    public Supplier getByPerson(Long id) {
        return supplierRepository.findSupplierByPerson_Id(id);
    }

    public Boolean countByPerson(Long id) {
        return supplierRepository.existsByPerson_Id(id);
    }

}
