package com.gdpsoft.nutrition.service;

import com.gdpsoft.nutrition.model.Measure;
import com.gdpsoft.nutrition.repository.MeasureRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MeasureService {

    @Autowired
    private MeasureRepository measureRepository;

    public Measure save(Measure day) {
        return measureRepository.save(day);
    }

    public Optional<Measure> get(Long id) {
        return measureRepository.findById(id);
    }

    public List<Measure> getAll() {
        return measureRepository.findAll();
    }

    public List<Measure> getAllByPatienId(Long id) {
        return measureRepository.findMeasuresByPatientId(id);
    }
}
