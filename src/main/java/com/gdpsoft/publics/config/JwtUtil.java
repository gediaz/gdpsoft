package com.gdpsoft.publics.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class JwtUtil {

    // Método para crear el JWT y enviarlo al cliente en el header de la respuesta
    static void addAuthentication(HttpServletResponse res, String username, Map<String, Object> body) throws IOException {
        System.out.println("JWT UTIL");
        String token = Jwts.builder()
                .setSubject(username)

                // Vamos a asignar un tiempo de expiracion de 1 minuto
                // solo con fines demostrativos en el video que hay al final
                .setExpiration(new Date(System.currentTimeMillis() + 36000000))
//                .setExpiration(new Date(System.currentTimeMillis() + 60000))

                // Hash con el que firmaremos la clave
                .signWith(SignatureAlgorithm.HS512, "test123456")
//                .signWith(SignatureAlgorithm.HS256, "secret".getBytes("UTF-8"))
                .compact();
        body.put("token", token);
        res.getWriter().write(new ObjectMapper().writeValueAsString(body));
        res.setStatus(200);
        //agregamos al encabezado el token
        res.addHeader("Authorization", "Bearer " + token);
//        req.setAttribute("aut", token);
    }

    // Método para validar el token enviado por el cliente
    static Authentication getAuthentication(HttpServletRequest request) throws Exception {

        // Obtenemos el token que viene en el encabezado de la peticion
        String token = request.getHeader("Authorization");

        // si hay un token presente, entonces lo validamos
        try {
            if (token != null) {
                String user = Jwts.parser()
                        .setSigningKey("test123456")
//                    .setSigningKey("P@tit0")
                        .parseClaimsJws(token.replace("Bearer", "")) //este metodo es el que valida
                        .getBody()
                        .getSubject();

                // Recordamos que para las demás peticiones que no sean /login
                // no requerimos una autenticacion por username/password
                // por este motivo podemos devolver un UsernamePasswordAuthenticationToken sin password
                return user != null ?
                        new UsernamePasswordAuthenticationToken(user, null, Collections.emptyList()) :
                        null;
            }

        } catch (io.jsonwebtoken.MalformedJwtException e) {
            throw new Exception("Token expiro");
        } catch (Exception e) {
            throw new Exception("Error");
        }
        return null;
    }


}
