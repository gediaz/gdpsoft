package com.gdpsoft.nutrition.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by gediaz on 7/24/2018.
 */
@Entity
@Table(name = "food_detail", schema = "nutrition")
public class FoodDetail implements Serializable {

    @Id
    @SequenceGenerator(name = "food_detail_sequence", sequenceName = "food_detail_id_seq", schema = "nutrition", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "food_detail_sequence")
    @Column(name = "food_detail_id")
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "food_id", updatable = false)
    @JsonIgnoreProperties("foodDetails")
    private Food food;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "food_category_id", columnDefinition = "BIGINT")
//    @JsonIgnore
    private FoodCategory category;

    @Column(name = "calorie")
    private String calorie;

    @Column(name = "proteins")
    private String proteins;

    @Column(name = "fats")
    private String fats;

    @Column(name = "carbohydrates")
    private String carbohydrates;

    @Column(name = "active")
    private Boolean active;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCalorie() {
        return calorie;
    }

    public void setCalorie(String calorie) {
        this.calorie = calorie;
    }

    public String getProteins() {
        return proteins;
    }

    public void setProteins(String proteins) {
        this.proteins = proteins;
    }

    public String getFats() {
        return fats;
    }

    public void setFats(String fats) {
        this.fats = fats;
    }

    public String getCarbohydrates() {
        return carbohydrates;
    }

    public void setCarbohydrates(String carbohydrates) {
        this.carbohydrates = carbohydrates;
    }

    public FoodCategory getCategory() {
        return category;
    }

    public void setCategory(FoodCategory category) {
        this.category = category;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Food getFood() {
        return food;
    }
}
