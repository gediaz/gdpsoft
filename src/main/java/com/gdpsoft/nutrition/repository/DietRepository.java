package com.gdpsoft.nutrition.repository;

import com.gdpsoft.nutrition.model.Diet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DietRepository extends JpaRepository<Diet, Long> {
    List<Diet> findDietsByPatientId(Long id);
}
