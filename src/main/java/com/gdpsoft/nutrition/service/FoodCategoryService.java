package com.gdpsoft.nutrition.service;

import com.gdpsoft.nutrition.model.FoodCategory;
import com.gdpsoft.nutrition.repository.FoodCategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class FoodCategoryService {

    @Autowired
    private FoodCategoryRepository foodCategoryRepository;

    public FoodCategory save(FoodCategory foodCategory) {
        return foodCategoryRepository.save(foodCategory);
    }

    public Optional<FoodCategory> get(Long id) {
        return foodCategoryRepository.findById(id);
    }

    public List<FoodCategory> getAll() {
        return foodCategoryRepository.findAll();
    }
}
