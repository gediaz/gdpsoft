package com.gdpsoft.publics.controller;

import com.gdpsoft.publics.model.User;
import com.gdpsoft.publics.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/api")
public class UserRest {

    @Autowired
    UserService userService;

    @PostMapping(value = "/user/add")
    public User add(@RequestBody User user) {
        return userService.save(user);
    }

    @PostMapping(value = "/user/update")
    public User update(@RequestBody User user) {
        return userService.save(user);
    }

    @GetMapping(value = {"/user/get", "/user"})
    public List<User> get() {
        return userService.getAll();
    }

//    @PostMapping(value = "/login")
//    public User validation(@RequestBody User user) throws Exception {
//        if (userService.userExit(user.getEmail(), user.getPassword())) {
//            return userService.getByEmailAndPassword(user.getEmail(), user.getPassword());
//        } else {
//            throw new Exception("El usuario no existe");
//        }
//    }
}
