package com.gdpsoft.miscellaneous.service;

import com.gdpsoft.miscellaneous.model.Gender;
import com.gdpsoft.miscellaneous.repository.GenderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class GenderService {

    @Autowired
    private GenderRepository genderRepository;

    public Gender save(Gender gender) {
        return genderRepository.save(gender);
    }


    /**
     * @param id
     * @return
     */
    public Optional<Gender> get(Long id) {
        return genderRepository.findById(id);
    }


    public List<Gender> getAll() {
        return genderRepository.findAll();
    }


}
