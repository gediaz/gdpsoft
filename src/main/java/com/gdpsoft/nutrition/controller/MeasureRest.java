package com.gdpsoft.nutrition.controller;

import com.gdpsoft.nutrition.model.Measure;
import com.gdpsoft.nutrition.service.MeasureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/api/measure")
public class MeasureRest {

    @Autowired
    MeasureService measureService;

    @PostMapping(value = "/add")
    public Measure add(@RequestBody Measure measure) {
        return measureService.save(measure);
    }

    @GetMapping(value = {"/get", "/"})
    public List<Measure> get() {
        return measureService.getAll();
    }

    @GetMapping(value = "/getByParentId")
    public List<Measure> getByPatientId(HttpServletRequest request) {
        Long id = Long.parseLong(request.getParameter("id"));
        return measureService.getAllByPatienId(id);
    }
}
