package com.gdpsoft.publics.controller;

import com.gdpsoft.publics.model.Menu;
import com.gdpsoft.publics.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/menu")
public class MenuRest {

    @Autowired
    MenuService menuService;

    @PostMapping(value = "/add")
    public Menu add(@RequestBody Menu menu) {
        return menuService.save(menu);
    }

    @GetMapping(value = {"/get", "/"})
    public List<Menu> get() {
        return menuService.getAll();
    }
}
