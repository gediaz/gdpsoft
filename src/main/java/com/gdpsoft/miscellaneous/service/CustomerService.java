package com.gdpsoft.miscellaneous.service;

import com.gdpsoft.miscellaneous.model.Customer;
import com.gdpsoft.miscellaneous.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CustomerService {

    @Autowired
    private CustomerRepository clientRepository;

    public Customer save(Customer client) {
        return clientRepository.save(client);
    }

    public List<Customer> getAll() {
        return clientRepository.findAll();
    }

    public Optional<Customer> get(Long id) {
        return clientRepository.findById(id);
    }

    public Customer getByPerson(Long id) {
        return clientRepository.findCustomerByPerson_Id(id);
    }

    public Boolean countByPerson(Long id) {
        return clientRepository.existsByPerson_Id(id);
    }

}
