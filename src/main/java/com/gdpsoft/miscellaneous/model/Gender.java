package com.gdpsoft.miscellaneous.model;

//import javax.persistence.Entity;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
@Table(schema = "miscellaneous", name = "gender")
public class Gender implements Serializable {

    @Id
    @SequenceGenerator(name = "gender_sequence", sequenceName = "gender_id_seq", schema = "miscellaneous", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gender_sequence")
    @Column(name = "gender_id")
    private Long id;

    @Column(name = "gender_name")
    private String name;

    @Column(name = "active")
    private Boolean active;

    @Column(name = "created_by")
    private Integer createdBy;

    @Column(name = "register_on", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable = true, updatable = false)
    private Timestamp registerDate;
//
//    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
//    @JoinColumn(name = "city_id")
//    private List<Person> Person;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(Timestamp registerDate) {
        this.registerDate = registerDate;
    }

    @Override
    public String toString() {
        return "Gender{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", active=" + active +
                ", createdBy=" + createdBy +
                ", registerDate=" + registerDate +
                '}';
    }
}
