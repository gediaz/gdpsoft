package com.gdpsoft.miscellaneous.repository;

import com.gdpsoft.miscellaneous.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepository extends JpaRepository<Person, Long> {

}
