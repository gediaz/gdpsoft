package com.gdpsoft.nutrition.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.gdpsoft.miscellaneous.model.Person;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

@Entity
@Table(schema = "nutrition", name = "measure")
public class Measure implements Serializable {

    @Id
    @SequenceGenerator(name = "measure_sequence", sequenceName = "measure_id_seq", schema = "nutrition", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "measure_sequence")
    @Column(name = "measure_id")
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "patient_id")
    private Patient patient;

    @Column(name = "height")
    private Double height;

    @Column(name = "weight")
    private Double weight;

    @Column(name = "bust")
    private Double bust;

    @Column(name = "arm")
    private Double arm;

    @Column(name = "thigh")
    private Double thigh;

    @Column(name = "waist")
    private Double waist;

    @Column(name = "hip")
    private Double hip;

    @Column(name = "active")
    private Boolean active;

    @Column(name = "created_by")
    private Integer createdBy;


    @Column(name = "measure_day")
//    @JsonFormat(pattern = "dd/MM/yyyy", timezone = "America/Santo_Domingo")
    private Date measureDay;

    @Column(name = "register_on", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable = true, updatable = false)
    private Timestamp registerDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Double getBust() {
        return bust;
    }

    public void setBust(Double bust) {
        this.bust = bust;
    }

    public Double getArm() {
        return arm;
    }

    public void setArm(Double arm) {
        this.arm = arm;
    }

    public Double getThigh() {
        return thigh;
    }

    public void setThigh(Double thigh) {
        this.thigh = thigh;
    }

    public Double getWaist() {
        return waist;
    }

    public void setWaist(Double waist) {
        this.waist = waist;
    }

    public Double getHip() {
        return hip;
    }

    public void setHip(Double hip) {
        this.hip = hip;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Date getMeasureDay() {
        return measureDay;
    }

    public void setMeasureDay(Date measureDay) {
        this.measureDay = measureDay;
    }

    public Timestamp getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(Timestamp registerDate) {
        this.registerDate = registerDate;
    }

    @PrePersist
    void onCreated() {
        this.setRegisterDate(new Timestamp(System.currentTimeMillis()));
        this.setActive(true);
    }

}
