package com.gdpsoft.nutrition.repository;

import com.gdpsoft.nutrition.model.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface PatientRepository extends JpaRepository<Patient, Long> {
    Patient findByPerson_Id(Long id);

    boolean existsByPerson_id(Long id);
}
