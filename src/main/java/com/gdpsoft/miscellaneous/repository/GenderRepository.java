package com.gdpsoft.miscellaneous.repository;

import com.gdpsoft.miscellaneous.model.Gender;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GenderRepository extends JpaRepository<Gender, Long> {

}
