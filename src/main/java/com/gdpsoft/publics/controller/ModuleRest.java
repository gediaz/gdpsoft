package com.gdpsoft.publics.controller;

import com.gdpsoft.publics.model.Menu;
import com.gdpsoft.publics.model.Module;
import com.gdpsoft.publics.service.MenuService;
import com.gdpsoft.publics.service.ModuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/module")
public class ModuleRest {

    @Autowired
    ModuleService moduleService;

    @PostMapping(value = "/add")
    public Module add(@RequestBody Module module) {
        return moduleService.save(module);
    }

    @GetMapping(value = {"/get", "/"})
    public List<Module> get() {
        return moduleService.getAll();
    }
}
