package com.gdpsoft.nutrition.service;

import com.gdpsoft.nutrition.model.Patient;
import com.gdpsoft.nutrition.repository.PatientRepository;
import com.gdpsoft.publics.model.Menu;
import com.gdpsoft.publics.repository.MenuRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PatientService {

    @Autowired
    private PatientRepository patientRepository;

    public Patient save(Patient patient) {
        return patientRepository.save(patient);
    }

    public Optional<Patient> get(Long id) {
        return patientRepository.findById(id);
    }

    public List<Patient> getAll() {
        return patientRepository.findAll();
    }

    public Patient getPersonById(Long id) {
        return patientRepository.findByPerson_Id(id);
    }

    public Boolean exitsPatientByPerson(Long id) {
        return patientRepository.existsByPerson_id(id);
    }
}
