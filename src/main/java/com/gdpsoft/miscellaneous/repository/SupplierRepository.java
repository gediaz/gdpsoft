package com.gdpsoft.miscellaneous.repository;

import com.gdpsoft.miscellaneous.model.Supplier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SupplierRepository extends JpaRepository<Supplier, Long> {

    public Supplier findSupplierByPerson_Id(Long id);

    Boolean existsByPerson_Id(Long id);
}
