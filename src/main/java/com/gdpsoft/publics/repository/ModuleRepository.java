package com.gdpsoft.publics.repository;

import com.gdpsoft.publics.model.Menu;
import com.gdpsoft.publics.model.Module;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ModuleRepository extends JpaRepository<Module, Long> {

//    List<Module> findAllByMenusByA_Apps();
}
