package com.gdpsoft.nutrition.controller;

import com.gdpsoft.nutrition.model.Diet;
import com.gdpsoft.nutrition.service.DietService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/api/diet")
public class DietRest {

    @Autowired
    DietService dietService;

    @PostMapping(value = "/add")
    public Diet add(@RequestBody Diet day) {
        return dietService.save(day);
    }

    @GetMapping(value = {"/get", "/"})
    public List<Diet> get() {
        return dietService.getAll();
    }

    @GetMapping(value = "/getByParentId")
    public List<Diet> getByPatientId(HttpServletRequest request) {
        Long id = Long.parseLong(request.getParameter("id"));
        return dietService.getAllByPatienId(id);
    }
}
