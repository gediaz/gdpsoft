package com.gdpsoft.miscellaneous.service;

import com.gdpsoft.miscellaneous.model.Person;
import com.gdpsoft.miscellaneous.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PersonService {

    @Autowired
    private PersonRepository personRepository;

    public Person save(Person person) {
        return personRepository.save(person);
    }

    public List<Person> getAll() {
        return personRepository.findAll();
    }

    public Optional<Person> get(Long id) {
        return personRepository.findById(id);
    }
}
