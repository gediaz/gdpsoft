package com.gdpsoft.publics.repository;

import com.gdpsoft.publics.model.Apps;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AppsRepository extends JpaRepository<Apps, Long> {

}
