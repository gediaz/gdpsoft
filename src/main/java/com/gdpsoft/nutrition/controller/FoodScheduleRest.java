package com.gdpsoft.nutrition.controller;

import com.gdpsoft.nutrition.model.FoodSchedule;
import com.gdpsoft.nutrition.service.FoodScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/food/schedule")
public class FoodScheduleRest {

    @Autowired
    FoodScheduleService foodScheduleService;

    @PostMapping(value = "/add")
    public FoodSchedule add(@RequestBody FoodSchedule foodSchedule) {
        return foodScheduleService.save(foodSchedule);
    }

    @GetMapping(value = {"/get", "/"})
    public List<FoodSchedule> get() {
        return foodScheduleService.getAll();
    }
}
