package com.gdpsoft.miscellaneous.controller;

import com.gdpsoft.miscellaneous.model.Person;
import com.gdpsoft.miscellaneous.service.CityService;
import com.gdpsoft.miscellaneous.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@RestController
@RequestMapping(value = "/api/person")
public class RestPerson {

    @Autowired
    PersonService personService;

    @Autowired
    CityService cityService;
//
//    @RequestMapping(value = "/", method = RequestMethod.GET)
//    public @ResponseBody
//    List<Person> index() {
//        return personService.getAll();
//    }
//
//    @RequestMapping(value = "/api/person/save", method = RequestMethod.GET)
//    public @ResponseBody
//    Person save() {
//        Person person = new Person();
//
//
//        person.setName("Geronimo");
//        person.setLastName("Diaz");
//        person.setActive(true);
//        Telephone telephone = new Telephone();
//        Telephone telephone1 = new Telephone();
//        telephone.setNumber("809-566-2806");
//        telephone.setActive(true);
//        telephone1.setNumber("809-566-2806");
//        telephone1.setActive(true);
//
//        List<Telephone> telephones = new List<Telephone>();
//        telephones.add(telephone);
//        telephones.add(telephone1);
//
//        person.setTelephones(telephones);
//
//
////        person.setTelephones();
////        personService.save(person);
//
//        cityService.get((long) 1).map(city1 -> {
//                    person.setCity(city1);
//                    return person;
//                }
//        );
//        return personService.save(person);
//    }

    @RequestMapping(value = {"/get", "/"}, method = RequestMethod.GET)
    public @ResponseBody
    List<Person> getAll() {
        return personService.getAll();
    }

    @RequestMapping(value = "/getid", method = RequestMethod.GET)
    public @ResponseBody
    Optional<Person> getId(HttpServletRequest request) {
        Long id = Long.parseLong(request.getParameter("id"));
        return personService.get(id);
    }

    @PostMapping(value = "/add", consumes = "application/json;charset=UTF-8")
    public @ResponseBody
    Optional<Person> add(@RequestBody Person person) {
        Long id = personService.save(person).getId();
        return personService.get(id);
    }


    @RequestMapping(value = "/getTest", method = RequestMethod.POST)
    public @ResponseBody
    String test(HttpServletRequest request) {
        Enumeration<String> headerNames = request.getParameterNames();
        while (headerNames.hasMoreElements()) {
            String headerName = headerNames.nextElement();
            System.out.println("Header Name - " + headerName + ", Value - " + request.getHeader(headerName));
        }
        return "KLK";
    }

}
