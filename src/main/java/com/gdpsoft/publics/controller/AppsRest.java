package com.gdpsoft.publics.controller;

import com.gdpsoft.publics.model.Apps;
import com.gdpsoft.publics.model.Menu;
import com.gdpsoft.publics.service.AppsService;
import com.gdpsoft.publics.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/apps")
public class AppsRest {

    @Autowired
    AppsService appsService;

    @PostMapping(value = "/add")
    public Apps add(@RequestBody Apps apps) {
        return appsService.save(apps);
    }

    @GetMapping(value = {"/get", "/"})
    public List<Apps> get() {
        return appsService.getAll();
    }
}
