package com.gdpsoft.miscellaneous.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by gediaz on 7/24/2018.
 */
@Entity
@Table(name = "telephone", schema = "miscellaneous")
public class Telephone implements Serializable {

    @Id
    @SequenceGenerator(name = "telephone_sequence", sequenceName = "telephone_id_seq", schema = "miscellaneous", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "telephone_sequence")
    @Column(name = "telephone_id")
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "person_id", updatable = false)
    private Person person;

    @Column(name = "number")
    private String number;

    @Column(name = "active")
    private Boolean active;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
//
//    public Person getPerson() {
//        return person;
//    }
//
//    public void setPerson(Person person) {
//        this.person = person;
//    }
}
