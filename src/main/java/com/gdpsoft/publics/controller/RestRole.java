package com.gdpsoft.publics.controller;

import com.gdpsoft.miscellaneous.model.Supplier;
import com.gdpsoft.miscellaneous.service.CityService;
import com.gdpsoft.miscellaneous.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/role")
public class RestRole {

    @Autowired
    CityService cityService;

    @Autowired
    SupplierService supplierService;

    @PostMapping(value = "/add")
    public @ResponseBody
    Supplier add(@RequestBody Supplier customer) throws Exception {
        Long personId = customer.getPerson().getId();
        if (supplierService.countByPerson(personId)) {
            throw new Exception("Ya esta persona es un cliente");
        } else {
            return supplierService.save(customer);
        }
    }

    @PostMapping(value = "/update")
    public Supplier update(@RequestBody Supplier supplier) {
        return supplierService.save(supplier);
    }

    @GetMapping(value = {"/get", "/"})
    public List<Supplier> get() {
        return supplierService.getAll();
    }
}
