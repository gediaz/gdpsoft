package com.gdpsoft.nutrition.service;

import com.gdpsoft.nutrition.model.Food;
import com.gdpsoft.nutrition.repository.FoodRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class FoodService {

    @Autowired
    private FoodRepository foodRepository;

    public Food save(Food food) {
        return foodRepository.save(food);
    }

    public Optional<Food> get(Long id) {
        return foodRepository.findById(id);
    }

    public List<Food> getAll() {
        return foodRepository.findAll();
    }

    public List<Food> getAllByName(String name) {
        return foodRepository.findAllByNameContaining(name);
    }
}
