package com.gdpsoft.miscellaneous.service;

import com.gdpsoft.miscellaneous.model.City;
import com.gdpsoft.miscellaneous.repository.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CityService {

    @Autowired
    private CityRepository cityRepository;

    public City save(City city) {
        return cityRepository.save(city);
    }


    /**
     *
     * @param id
     * @return
     */
    public Optional<City> get(Long id) {

//        Optional<City> cityOptional
        return cityRepository.findById(id);
    }


    public List<City> getAll() {
        return cityRepository.findAll();
    }


}
