package com.gdpsoft.nutrition.service;

import com.gdpsoft.nutrition.model.Diet;
import com.gdpsoft.nutrition.repository.DietRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DietService {

    @Autowired
    private DietRepository dietRepository;

    public Diet save(Diet day) {
        return dietRepository.save(day);
    }

    public Optional<Diet> get(Long id) {
        return dietRepository.findById(id);
    }

    public List<Diet> getAll() {
        return dietRepository.findAll();
    }

    public List<Diet> getAllByPatienId(Long id) {
        return dietRepository.findDietsByPatientId(id);
    }
}
