package com.gdpsoft.publics.service;

import com.gdpsoft.publics.model.Menu;
import com.gdpsoft.publics.repository.MenuRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MenuService {

    @Autowired
    private MenuRepository menuRepository;

    public Menu save(Menu role) {
        return menuRepository.save(role);
    }

    public Optional<Menu> get(Long id) {
        return menuRepository.findById(id);
    }

    public List<Menu> getAll() {
        return menuRepository.findAllByOrderByOrderAsc();
    }

}
