package com.gdpsoft.publics.service;

import com.gdpsoft.publics.model.Apps;
import com.gdpsoft.publics.repository.AppsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AppsService {

    @Autowired
    private AppsRepository appsRepository;

    public Apps save(Apps apps) {
        return appsRepository.save(apps);
    }

    public Optional<Apps> get(Long id) {
        return appsRepository.findById(id);
    }

    public List<Apps> getAll() {
        return appsRepository.findAll();
    }
}
