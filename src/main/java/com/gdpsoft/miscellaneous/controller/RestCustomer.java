package com.gdpsoft.miscellaneous.controller;

import com.gdpsoft.miscellaneous.model.Customer;
import com.gdpsoft.miscellaneous.model.Person;
import com.gdpsoft.miscellaneous.model.Telephone;
import com.gdpsoft.miscellaneous.service.CityService;
import com.gdpsoft.miscellaneous.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api/customer")
public class RestCustomer {

    @Autowired
    CityService cityService;

    @Autowired
    CustomerService customerService;
//
//    @RequestMapping(value = "/save", method = RequestMethod.GET)
//    public @ResponseBody
//    Customer index() {
//        Customer client = new Customer();
//        Person person = new Person();
//
//
//        person.setName("Geronimo");
//        person.setLastName("Diaz");
//        person.setActive(true);
//        Telephone telephone = new Telephone();
//        Telephone telephone1 = new Telephone();
//        telephone.setNumber("809-566-2806");
//        telephone.setActive(true);
//        telephone1.setNumber("809-566-2806");
//        telephone1.setActive(true);
//
//        Set<Telephone> telephones = new HashSet<Telephone>();
//        telephones.add(telephone);
//        telephones.add(telephone1);
//
//        person.setTelephones(telephones);
//
//        cityService.get((long) 1).map(city1 -> {
//                    person.setCity(city1);
//                    return person;
//                }
//        );
//        client.setLimit((double) 5000);
//        client.setPerson(person);
//        customerService.save(client);
//
////        client.set
//        return client;
//    }

    @PostMapping(value = "/add")
    public @ResponseBody
    Customer add(@RequestBody Customer customer) throws Exception {
        Long personId = customer.getPerson().getId();
        if (customerService.countByPerson(personId)) {
            throw new Exception("Ya esta persona es un cliente");
        } else {
            return customerService.save(customer);
        }
    }

    @PostMapping(value = "/update")
    public @ResponseBody
    Customer update(@RequestBody Customer customer) {
        return customerService.save(customer);
    }

    @RequestMapping(value = {"/get", "/"}, method = RequestMethod.GET)
    public @ResponseBody
    List<Customer> getAll() {
        return customerService.getAll();
    }

    @RequestMapping(value = "/getCustomerByPersonId", method = RequestMethod.GET)
    public @ResponseBody
    Customer getByPersonId(HttpServletRequest request) {
        Long personId = Long.parseLong(request.getParameter("id"));
        return customerService.getByPerson(personId);
    }

}
