package com.gdpsoft.nutrition.model;

//import javax.persistence.Entity;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Table(schema = "nutrition", name = "diet_schedule")
public class DietDaySchedule implements Serializable {

    @Id
    @SequenceGenerator(name = "diet_schedule_sequence", sequenceName = "diet_schedule_id_seq", schema = "nutrition", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "diet_schedule_sequence")
    @Column(name = "diet_schedule_id")
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "food_schedule_id")
    private FoodSchedule schedule;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "diet_schedule_id")
    private List<FoodDiet> foodDiets;

    @Column(name = "active")
    private Boolean active;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public FoodSchedule getSchedule() {
        return schedule;
    }

    public void setSchedule(FoodSchedule schedule) {
        this.schedule = schedule;
    }

    public List<FoodDiet> getFoodDiets() {
        return foodDiets;
    }

    public void setFoodDiets(List<FoodDiet> foodDiets) {
        this.foodDiets = foodDiets;
    }
}
