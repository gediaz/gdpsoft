package com.gdpsoft.miscellaneous.service;

import com.gdpsoft.miscellaneous.model.Document;
import com.gdpsoft.miscellaneous.repository.DocumentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DocumentService {

    @Autowired
    private DocumentRepository documentRepository;

    public Document save(Document document) {
        return documentRepository.save(document);
    }


    /**
     *
     * @param id
     * @return
     */
    public Optional<Document> get(Long id) {

//        Optional<Document> documentOptional
        return documentRepository.findById(id);
    }


    public List<Document> getAll() {
        return documentRepository.findAll();
    }


}
