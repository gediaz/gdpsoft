package com.gdpsoft.publics.service;

import com.gdpsoft.publics.model.Menu;
import com.gdpsoft.publics.model.Module;
import com.gdpsoft.publics.repository.MenuRepository;
import com.gdpsoft.publics.repository.ModuleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ModuleService {

    @Autowired
    private ModuleRepository moduleRepository;

    public Module save(Module role) {
        return moduleRepository.save(role);
    }

    public Optional<Module> get(Long id) {
        return moduleRepository.findById(id);
    }

    public List<Module> getAll() {
        return moduleRepository.findAll();
    }

}
