package com.gdpsoft.nutrition.repository;

import com.gdpsoft.nutrition.model.FoodSchedule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FoodScheduleRepository extends JpaRepository<FoodSchedule, Long> {

}
