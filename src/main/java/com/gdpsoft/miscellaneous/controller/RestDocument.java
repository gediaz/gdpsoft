package com.gdpsoft.miscellaneous.controller;

import com.gdpsoft.miscellaneous.model.Document;
import com.gdpsoft.miscellaneous.service.DocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/document")
public class RestDocument {

    @Autowired
    DocumentService documentService;

    @RequestMapping(value = {"/get", "/"}, method = RequestMethod.GET)
    public @ResponseBody
    List<Document> getAll() {
        return documentService.getAll();
    }

    @PostMapping(value = "/add")
    public @ResponseBody
    Document add(@RequestBody Document document) {
        return documentService.save(document);
    }
}
