package com.gdpsoft.publics.service;

import com.gdpsoft.publics.model.Roles;
import com.gdpsoft.publics.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RoleService {

    @Autowired
    private RoleRepository roleRepository;

    public Roles save(Roles role) {
        return roleRepository.save(role);
    }


    /**
     * @param id
     * @return
     */
    public Optional<Roles> get(Long id) {
        return roleRepository.findById(id);
    }


    public List<Roles> getAll() {
        return roleRepository.findAll();
    }


}
