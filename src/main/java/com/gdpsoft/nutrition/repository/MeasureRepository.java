package com.gdpsoft.nutrition.repository;

import com.gdpsoft.nutrition.model.Measure;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MeasureRepository extends JpaRepository<Measure, Long> {
    List<Measure> findMeasuresByPatientId(Long id);
}
