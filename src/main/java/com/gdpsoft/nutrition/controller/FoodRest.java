package com.gdpsoft.nutrition.controller;

import com.gdpsoft.nutrition.model.Food;
import com.gdpsoft.nutrition.service.FoodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/api/food")
public class FoodRest {

    @Autowired
    FoodService foodService;

    @PostMapping(value = "/add")
    public Food add(@RequestBody Food foodCategory) {
        return foodService.save(foodCategory);
    }

    @GetMapping(value = {"/get", "/"})
    public List<Food> get() {
        return foodService.getAll();
    }

    @GetMapping(value = "/get/name")
    public List<Food> getByName(HttpServletRequest request) {
        return foodService.getAllByName(request.getParameter("name"));
    }
}
