package com.gdpsoft.nutrition.model;

//import javax.persistence.Entity;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Table(schema = "nutrition", name = "diet_day")
public class DietDay implements Serializable {

    @Id
    @SequenceGenerator(name = "diet_day_sequence", sequenceName = "diet_day_id_seq", schema = "nutrition", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "diet_day_sequence")
    @Column(name = "diet_day_id")
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "day_id")
    private Day day;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "diet_day_id")
    private List<DietDaySchedule> dietDaySchedules;

    @Column(name = "active")
    private Boolean active;

    @Column(name = "created_by")
    private Integer createdBy;

    @Column(name = "register_on", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable = true, updatable = false)
    private Timestamp registerDate;
//
//    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
//    @JoinColumn(name = "city_id")
//    private List<Person> Person;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Day getDay() {
        return day;
    }

    public void setDay(Day day) {
        this.day = day;
    }

    public List<DietDaySchedule> getDietDaySchedules() {
        return dietDaySchedules;
    }

    public void setDietDaySchedules(List<DietDaySchedule> dietDaySchedules) {
        this.dietDaySchedules = dietDaySchedules;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(Timestamp registerDate) {
        this.registerDate = registerDate;
    }

    @Override
    public String toString() {
        return "DietDay{" +
                "id=" + id +
                ", active=" + active +
                ", createdBy=" + createdBy +
                ", registerDate=" + registerDate +
                '}';
    }
}
