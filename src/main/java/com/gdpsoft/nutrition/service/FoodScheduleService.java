package com.gdpsoft.nutrition.service;

import com.gdpsoft.nutrition.model.Day;
import com.gdpsoft.nutrition.model.FoodSchedule;
import com.gdpsoft.nutrition.repository.DayRepository;
import com.gdpsoft.nutrition.repository.FoodScheduleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class FoodScheduleService {

    @Autowired
    private FoodScheduleRepository foodScheduleRepository;

    public FoodSchedule save(FoodSchedule day) {
        return foodScheduleRepository.save(day);
    }

    public Optional<FoodSchedule> get(Long id) {
        return foodScheduleRepository.findById(id);
    }

    public List<FoodSchedule> getAll() {
        return foodScheduleRepository.findAll();
    }
}
