package com.gdpsoft.publics.service;

import com.gdpsoft.publics.config.UserPrincipal;
import com.gdpsoft.publics.model.User;
import com.gdpsoft.publics.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class UserService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    public User save(User user) {
        return userRepository.save(user);
    }

    public Optional<User> get(Long id) {
        return userRepository.findById(id);
    }

    public User getByEmailAndPassword(String email, String password) {
        return userRepository.findByEmailAndPassword(email, password);
    }

    public List<User> getAll() {
        return userRepository.findAll();
    }

    public Boolean userExit(String email, String password) {
        return userRepository.existsUserByEmailAndPassword(email, password);
    }

    public Boolean userExitByUserName(String userName, String password) {
        return userRepository.existsUserByEmailAndPassword(userName, password);
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String usernameOrEmail) throws UsernameNotFoundException {
        // Let people login with either username or email
        User user = userRepository.findByUsernameOrEmail(usernameOrEmail, usernameOrEmail);
        System.out.println("usernameOrEmail = [" + usernameOrEmail + "]");
        System.out.println("userService = [" + user.toString() + "]");
        return UserPrincipal.create(user);
    }

    // This method is used by JWTAuthenticationFilter
    @Transactional
    public UserDetails loadUserById(Long id) {
        User user = userRepository.findById(id).orElseThrow(
                () -> new UsernameNotFoundException("User not found with id : " + id)
        );
        return UserPrincipal.create(user);
    }
}
