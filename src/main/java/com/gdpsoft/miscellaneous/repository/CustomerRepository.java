package com.gdpsoft.miscellaneous.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.gdpsoft.miscellaneous.model.Customer;

import java.util.List;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {

    public Customer findCustomerByPerson_Id(Long id);

    Boolean existsByPerson_Id(Long id);
}
