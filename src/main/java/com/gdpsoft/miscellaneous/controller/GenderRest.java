package com.gdpsoft.miscellaneous.controller;

import com.gdpsoft.miscellaneous.model.Gender;
import com.gdpsoft.miscellaneous.service.GenderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/gender")
public class GenderRest {

    @Autowired
    GenderService genderService;

    @RequestMapping(value = {"/get", "/"}, method = RequestMethod.GET)
    public List<Gender> getAll() {
        return genderService.getAll();
    }

    @PostMapping(value = "/add")
    public Gender add(@RequestBody Gender gender) {
        return genderService.save(gender);
    }
}
