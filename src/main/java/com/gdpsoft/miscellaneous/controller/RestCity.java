package com.gdpsoft.miscellaneous.controller;

import com.gdpsoft.miscellaneous.model.City;
import com.gdpsoft.miscellaneous.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/city")
public class RestCity {

    @Autowired
    CityService cityService;

    @RequestMapping(value = "/save", method = RequestMethod.GET)
    public @ResponseBody
    City index() {
        City city = new City();
        city.setName("Vega");
        city.setCreatedBy(1);
        city.setActive(true);
        city.setRegisterDate(new Timestamp(System.currentTimeMillis()));

        return cityService.save(city);
    }

    @RequestMapping(value = {"/get", "/"}, method = RequestMethod.GET)
    public @ResponseBody
    List<City> getAll() {
        return cityService.getAll();
    }

    @PostMapping(value = "/add")
    public @ResponseBody
    City add(@RequestBody City city) {
        return cityService.save(city);
    }
}
