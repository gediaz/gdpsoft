package com.gdpsoft.nutrition.model;

//import javax.persistence.Entity;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Table(schema = "nutrition", name = "diet")
public class Diet implements Serializable {

    @Id
    @SequenceGenerator(name = "diet_sequence", sequenceName = "diet_id_seq", schema = "nutrition", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "diet_sequence")
    @Column(name = "diet_id")
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "patient_id")
    private Patient patient;

    @Column(name = "diet_name")
    private String name;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "diet_id")
    private List<DietDay> dietDays;

    @Column(name = "active")
    private Boolean active;

    @Column(name = "created_by")
    private Integer createdBy;

    @Column(name = "register_on", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable = true, updatable = false)
    private Timestamp registerDate;
//
//    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
//    @JoinColumn(name = "city_id")
//    private List<Person> Person;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<DietDay> getDietDays() {
        return dietDays;
    }

    public void setDietDays(List<DietDay> dietDays) {
        this.dietDays = dietDays;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(Timestamp registerDate) {
        this.registerDate = registerDate;
    }

}
