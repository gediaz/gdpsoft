package com.gdpsoft.nutrition.controller;

import com.gdpsoft.nutrition.model.FoodCategory;
import com.gdpsoft.nutrition.model.Patient;
import com.gdpsoft.nutrition.service.FoodCategoryService;
import com.gdpsoft.nutrition.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/food/category")
public class FoodCategoryRest {

    @Autowired
    FoodCategoryService foodCategoryService;

    @PostMapping(value = "/add")
    public FoodCategory add(@RequestBody FoodCategory foodCategory) {
        return foodCategoryService.save(foodCategory);
    }

    @GetMapping(value = {"/get", "/"})
    public List<FoodCategory> get() {
        return foodCategoryService.getAll();
    }
}
