package com.gdpsoft.nutrition.service;

import com.gdpsoft.nutrition.model.Day;
import com.gdpsoft.nutrition.repository.DayRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DayService {

    @Autowired
    private DayRepository dayRepository;

    public Day save(Day day) {
        return dayRepository.save(day);
    }

    public Optional<Day> get(Long id) {
        return dayRepository.findById(id);
    }

    public List<Day> getAll() {
        return dayRepository.findAll();
    }
}
