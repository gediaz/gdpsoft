package com.gdpsoft.nutrition.controller;

import com.gdpsoft.nutrition.model.Day;
import com.gdpsoft.nutrition.service.DayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/api/food/day")
public class FoodDayRest {

    @Autowired
    DayService dayService;

    @PostMapping(value = "/add")
    public Day add(@RequestBody Day day) {
        return dayService.save(day);
    }

    @GetMapping(value = {"/get", "/"})
    public List<Day> get() {
        return dayService.getAll();
    }
}
