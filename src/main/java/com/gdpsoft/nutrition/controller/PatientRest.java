package com.gdpsoft.nutrition.controller;

import com.gdpsoft.nutrition.model.Patient;
import com.gdpsoft.nutrition.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/patient")
public class PatientRest {

    @Autowired
    PatientService patientService;

    @PostMapping(value = "/add")
    public Patient add(@RequestBody Patient patient) throws Exception {
        Long personId = patient.getPerson().getId();
        if (patientService.exitsPatientByPerson(personId)) {
            throw new Exception("Ya el paciente existe");
        } else {
            return patientService.save(patient);
        }
    }

    @PostMapping(value = "/update")
    public Patient update(@RequestBody Patient patient) {
        return patientService.save(patient);
    }

    @GetMapping(value = {"/get", "/"})
    public List<Patient> get() {
        return patientService.getAll();
    }
}
