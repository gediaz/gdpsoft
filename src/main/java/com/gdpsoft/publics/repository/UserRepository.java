package com.gdpsoft.publics.repository;

import com.gdpsoft.publics.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    Boolean existsUserByEmailAndPassword(String email, String password);

    Boolean existsUserByUsernameAndPassword(String userName, String password);

    User findByEmailAndPassword(String email, String password);

    User findByUsernameOrEmail(String userName, String email);
}
